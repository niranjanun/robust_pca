% This matlab script can be used to reproduce the synthetic results in the NcRPCA
% paper, in the case of varying sparsity. This compares the NcRPCA with the
% IALM method for solving the low rank+sparse matrix decomposition.
% Parameters such as sparsity, rank, incoherence can be varied as desired.

clear;
for avg_runs = 1:5 % averaging over many runs
i_d = 1
for d = 450:50:850 % varying sparsity
clearvars -except avg_runs i_d d time_d err_d max_rank_d;
clc;

m = 2000; % problem size
n = 2000;

r = 5; % rank of low-rank component
incoh = 1; % incoherence parameter
tune_lambda = 0;

fact = (1-(1/(m*n)))/(r-1);
vari = 0.05;
for i = 1:r-1
   cent = 1-(i-1)*fact;
   Sig(i, i) = 1;%cent*(1 + vari*cent*randn);
end
Sig(r, r) = 1;%/(m*n); % set condition number

zU = zeros(m,r);
zV = zeros(n,r);
rp = randperm(m);
z_u = floor(m/incoh^2);
zU(rp(1:z_u), :) = randn(z_u, r);
rp = randperm(n);
z_v = floor(n/incoh^2);
zV(rp(1:z_v), :) = randn(z_v, r);
zU = zU./repmat(sqrt(sum(zU.*zU,1)),m,1);
zV = zV./repmat(sqrt(sum(zV.*zV,1)),n,1); % set incoherence
L = zU*Sig*zV';
condn_no = Sig(1,1)/Sig(end,end)

%d = r*4; % degree
p = d/sqrt(m*n); % generate sparse component
S = rand(m, n);
s_zero_idx = find(S>p);
s_nonzero_idx = find(S<=p);
S(s_zero_idx) = 0;
S(s_nonzero_idx) = (r/sqrt(m*n))*(rand(length(s_nonzero_idx),1)/2+.5).*sign(randn(length(s_nonzero_idx),1)); 
%S(1,1) = 1000; % to test initial thresholding for large outliers

M = L+S; % input for NcRPCA: low-rank+sparse matrix

EPS = 1e-3;
EPS_S = 1e-3;
MAX_ITER = 51;
lambda = 1/sqrt(m);

tic;
[L_t_ncrpca, S_t_ncrpca, iter_ncrpca, frob_err_ncrpca] = ncrpca(M, r, EPS, MAX_ITER, EPS_S, incoh);
time_ncrpca = toc

if tune_lambda == 1 % tune for the regularization in convex RPCA
    l = 1;
    npts = 3; % number of grid points
    for lambda = linspace(1/sqrt(m), 3/sqrt(m), npts)
        tic;
        [cell_A_hat{l}, cell_E_hat{l}, cell_iter_alm{l}, cell_frob_err_alm{l}, cell_rank_iters{l}] = inexact_alm_rpca(M, lambda, EPS, MAX_ITER);
        cell_time_alm{l} = toc
        l = l+1;
    end
    for l = 1:length(cell_iter_alm)
        arr_err_alm(l) = norm(cell_A_hat{l}-L, 'fro') / norm(L, 'fro');
        [min_err_L min_err_L_idx] = min(arr_err_alm);
    end
    A_hat = cell_A_hat{min_err_L_idx};
    E_hat = cell_E_hat{min_err_L_idx};
    iter_alm = cell_iter_alm{min_err_L_idx};
    frob_err_alm = cell_frob_err_alm{min_err_L_idx};
    rank_iters = cell_rank_iters{min_err_L_idx};
    time_alm = cell_time_alm{min_err_L_idx};
else
    tic;
    [A_hat, E_hat, iter_alm, frob_err_alm, rank_iters] = inexact_alm_rpca(M, lambda, EPS, MAX_ITER);
    time_alm = toc
end

err_ncrpca = norm(L_t_ncrpca-L, 'fro') / norm(L, 'fro')
err_alm = norm(A_hat-L, 'fro') / norm(L, 'fro')
rank_L_t_ncrpca = rank(L_t_ncrpca)
rank_A_hat = rank(A_hat)
sparsity_S_t_ncrpca = nnz(S_t_ncrpca)
sparsity_E_hat = nnz(E_hat)
sparsity_S = nnz(S)

time_d(i_d, 1, avg_runs) = time_ncrpca;
time_d(i_d, 2, avg_runs) = time_alm;

err_d(i_d, 1, avg_runs) = err_ncrpca;
err_d(i_d, 2, avg_runs) = err_alm;

max_rank_d(i_d, avg_runs) = max(rank_iters);

save(['avg_run' num2str(avg_runs) 'm' num2str(m) 'n' num2str(n) 'r' num2str(r) 'incoh' num2str(incoh) 'd' num2str(d) 'cno' num2str(condn_no) 'EPS' num2str(EPS) '.mat']);
i_d = i_d+1;
end
end
save('time_d.mat','time_d'); % save outputs for plotting later
save('err_d.mat','err_d');
save('max_rank_d.mat','max_rank_d');
